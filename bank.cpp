#include <iostream>
#include <vector>
#include <algorithm>
#include "bank.h"
using namespace std;

Bank::~Bank() {};

void Bank::push(int a, int s) {

		Customer *tmp = head;

		if (head != 0) {
			while (tmp != tail) {
				tmp = tmp->next;
			}

			tail = new Customer(a,s);

			tmp->next = tail;
		} else {

			tail = new Customer(a,s);

			head = tail;
		}
}

void Bank::pop() {
	
	Customer *tmp = head;

	if (head == tail){
		head = tail = 0;
	}else{
		head = tmp->next;
	}

	delete tmp;
	
	return;
}

void Bank::genarate(int service_min, int service_max, int customer_min, int customer_max, int open_time, int seed) {

	int customer_diff = customer_max - customer_min;
	int service_diff = service_max - service_min;

	int random_seed = seed;

	srand(random_seed);
	random_seed++;

	int customer = (rand() % customer_diff) + customer_min;

	int *customer_arrived = new int[customer];

	for (int i = 0; i <= customer; i++) {

		srand(random_seed);
		random_seed = (random_seed*(random_seed+1));

		customer_arrived[i] = (rand() % open_time);

	}

	sort(customer_arrived, customer_arrived + customer);

	for (int i = 0; i <= customer; i++) {

		int arrival = customer_arrived[i];

		int service_time = (rand() % service_diff) + service_min;

		this->push(arrival, service_time);

	}


}

void Bank::execute(int NumCashiers) {

	int *next = new int[NumCashiers];
	int *wait = new int[NumCashiers];
	int arrived;
	int service;
	int counter=1;
	int select=0;
	int customer = 1;
	int sumwait = 0;

	for (int i=0; i < NumCashiers; i++) {
		next[i] = 0;
		wait[i] = 0;
	}

	while (head != tail) {

		select = (counter%NumCashiers);

		arrived = head->arrived();
		service = head->service();

		if (arrived < next[select]) {

			counter++;
			select = (counter%NumCashiers);

			wait[select] = (next[select] - arrived);
			if (wait[select] < 0) {
				next[select] = arrived + service;
			} else {
				next[select] = wait[select] + arrived + service;
			}
		} else {
			next[select] = arrived + service;
		}

		if (wait[select] < 0) {
			cout << "customer " << customer << " arrived : " << arrived << " wait : 0" << " service time : " << service << " next : " << next[select] << endl;
		} else {
			cout << "customer " << customer << " arrived : " << arrived << " wait : " << wait[select] << " service time : " << service << " next : " << next[select] << endl;
			sumwait = sumwait + wait[select];
		}

		customer++;
		wait[select] = 0;
		pop();
	}

	cout << "avg wait :" << (float)sumwait / counter;

}
