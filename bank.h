#pragma once
#ifndef BANK
#define BANK

class Customer {
public:
	Customer *next;
	Customer() { }
	Customer(int a, int s) {
		this->customer_arrived = a;
		this->customer_service = s;
	}
	int arrived() {
		return this->customer_arrived;
	}
	int service() {
		return this->customer_service;
	}
private:
	int customer_arrived; // arrived time
	int customer_service; // service time
};


class Bank{
public:
	Bank() {};
	void genarate(int,int,int,int,int,int); // genarate environment
	void execute(int); // simulate 
	bool isEmpty() { return head == 0; }
	~Bank();

	void push(int, int);
	void pop();

private:
	Customer *head,*tail;
};

#endif