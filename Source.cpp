#include <iostream>
#include <ctime>
#include <cstdlib>
#include "bank.h"
using namespace std;

void main() {

	Bank bank;

	int MaxCustomers, MinCustomers, MaxServiceTime, MinServiceTime, NumCashiers;
	cout << "Enter your MaxCustomers = "; //input
	cin >> MaxCustomers;
	cout << "Enter your MinCustomers = ";
	cin >> MinCustomers;
	cout << "Enter your MaxServiceTime = ";
	cin >> MaxServiceTime;
	cout << "Enter your MinServiceTime = ";
	cin >> MinServiceTime;
	cout << "Enter your NumCashiers = ";
	cin >> NumCashiers;

	bank.genarate(MinServiceTime,MaxServiceTime,MinCustomers,MaxCustomers,240,time(NULL));

	bank.execute(NumCashiers);

	cout << endl;

	system("pause");

}